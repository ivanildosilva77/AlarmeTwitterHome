#include <SPI.h> // needed in Arduino 0019 or later
#include <Ethernet.h>
#include <Twitter.h>

//Declaração das variáveis referentes aos pinos digitais.
int pinBuzzer = 7;
int pinSensorPIR = 8;
int pinLed = 9;
int valorSensorPIR = 0;
int controle =1;
// Ethernet Shield Settings
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0x83, 0xEA }; // Entre com o valor do MAC

// If you don't specify the IP address, DHCP is used(only in Arduino 1.0 or later).
byte ip[] = { 10, 0, 0, 2 };

// Your Token to Tweet (get it from http://arduino-tweet.appspot.com/)
//silvashome
Twitter twitter("2701806360-ywNAHABtcl6OKT3reZ0sQTazgG1qCAYKttGUCoI");
int i=1; // start with zero
char buf[100];

void setup() {
  Serial.begin(9600); //Inicializando o serial monitor

  //Definido pinos como de entrada ou de saída
  pinMode(pinBuzzer,OUTPUT);
  pinMode(pinSensorPIR,INPUT);
  pinMode(pinLed,OUTPUT);
  delay(1000);
  Ethernet.begin(mac, ip);
}

void loop() {   

  //Lendo o valor do sensor PIR. Este sensor pode assumir 2 valores
  //1 quando detecta algum movimento e 0 quando não detecta.
  valorSensorPIR = digitalRead(pinSensorPIR);
  
  Serial.print("Valor do Sensor PIR: ");  
  Serial.println(valorSensorPIR);
  
  //Verificando se ocorreu detecção de movimentos
    if (valorSensorPIR == 1) {
      ligarAlarme();
      sprintf(buf, "Seu Arduino detectou movimento em sua casa. Aviso numero %d via an #Arduino Ethernet Board!", i); 
      tweet(buf);
      i++;
      Serial.println(i);
      // zero delay
      delay(15000);
  } else {
    desligarAlarme();
  }    
}
void ligarAlarme() {
  //Ligando o led
  digitalWrite(pinLed, HIGH);
  
  //Ligando o buzzer com uma frequencia de 1500 hz.
  tone(pinBuzzer,1500);
  
  delay(4000); //tempo que o led fica acesso e o buzzer toca
  
  desligarAlarme();
}

void desligarAlarme() {
  //Desligando o led
  digitalWrite(pinLed, LOW);
  
  //Desligando o buzzer
  noTone(pinBuzzer);
}
void tweet(char msg[]) {
  Serial.println("connecting ...");
  if (twitter.post(msg)) {
    int status = twitter.wait(&Serial);
    if (status == 200) {
      Serial.println("OK.");
    } else {
      Serial.print("failed : code ");
      Serial.println(status);
    }
  } else {
    Serial.println("connection failed.");
  }
}
